package signer

import (
	"crypto"
	"crypto/x509"
	"fmt"
	"path"

	"git.cacert.org/cacert-gosigner/shared"
	"git.cacert.org/cacert-gosigner/signer/openpgpops"
	"git.cacert.org/cacert-gosigner/signer/x509ops"
)

const (
	CsX509    shared.CryptoSystemID = 1
	CsOpenPGP shared.CryptoSystemID = 2
)

const (
	X509RootDefault shared.CryptoSystemRootID = 0
	X509RootClass3  shared.CryptoSystemRootID = 1
	// The following roots existed in the old server.pl but had
	// no profile configurations and were thus unusable
	//
	// X509RootClass3s shared.CryptoSystemRootID = 2
	// X509Root3       shared.CryptoSystemRootID = 3
	// X509Root4       shared.CryptoSystemRootID = 4
	// X509Root5       shared.CryptoSystemRootID = 5
)

const (
	X509ProfileClient         shared.CertificateProfileID = 0
	X509ProfileClientOrg      shared.CertificateProfileID = 1
	X509ProfileClientCodesign shared.CertificateProfileID = 2
	X509ProfileServer         shared.CertificateProfileID = 5
	X509ProfileServerOrg      shared.CertificateProfileID = 6
	X509ProfileOCSP           shared.CertificateProfileID = 8
	X509ProfileTimestamp      shared.CertificateProfileID = 9

	// the following profiles where valid options in the original signer code but had no configurations
	//
	// X509ProfileClientMachine  shared.CertificateProfileID = 3  // no configuration on original signer
	// X509ProfileClientAds      shared.CertificateProfileID = 4  // no configuration on original signer
	// X509ProfileServerJabber   shared.CertificateProfileID = 7  // no configuration on original signer
	// X509ProfileProxy          shared.CertificateProfileID = 10  // no configuration on original signer
	// X509ProfileSubCA          shared.CertificateProfileID = 11  // no configuration on original signer
)

const (
	X509MDDefault shared.SignatureAlgorithmID = 0
	X509MDMd5     shared.SignatureAlgorithmID = 1
	X509MDSha1    shared.SignatureAlgorithmID = 2
	// X509MDRipeMD160 shared.SignatureAlgorithmID = 3  x509ops package does not support RIPEMD160
	X509MDSha256 shared.SignatureAlgorithmID = 8
	X509MDSha384 shared.SignatureAlgorithmID = 9
	X509MDSha512 shared.SignatureAlgorithmID = 10
)

const (
	OpenPGPRoot0 shared.CryptoSystemRootID = 0
)

const (
	OpenPGPDefaultProfile shared.CertificateProfileID = 0
)

const (
	OpenPGPDefaultMD shared.SignatureAlgorithmID = 0
)

func NewCommandProcessor() *CommandProcessor {
	settings := NewCommandProcessorSettings()

	clientPrototype := &x509.Certificate{
		KeyUsage: x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageEmailProtection,
			x509.ExtKeyUsageClientAuth,
			// x509ops.ExtKeyUsageMicrosoftServerGatedCrypto,
			// 1.3.6.1.4.1.311.10.3.4 msEFS not supported by golang.org/crypto
			// x509ops.ExtKeyUsageNetscapeServerGatedCrypto,
		},
	}
	codeSignPrototype := &x509.Certificate{
		KeyUsage: x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageEmailProtection,
			x509.ExtKeyUsageClientAuth,
			x509.ExtKeyUsageCodeSigning,
			// 1.3.6.1.4.1.311.2.1.21 msCodeInd not supported by golang.org/crypto
			// x509ops.ExtKeyUsageMicrosoftCommercialCodeSigning,
			// x509ops.ExtKeyUsageMicrosoftServerGatedCrypto,
			// 1.3.6.1.4.1.311.10.3.4 msEFS not supported by golang.org/crypto
			// x509ops.ExtKeyUsageNetscapeServerGatedCrypto,
		},
	}
	serverPrototype := &x509.Certificate{
		KeyUsage: x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageClientAuth,
			x509.ExtKeyUsageServerAuth,
			// x509ops.ExtKeyUsageMicrosoftServerGatedCrypto,
			// x509ops.ExtKeyUsageNetscapeServerGatedCrypto,
		},
	}
	ocspPrototype := &x509.Certificate{
		KeyUsage: x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageServerAuth,
			x509.ExtKeyUsageOCSPSigning,
			// x509ops.ExtKeyUsageMicrosoftServerGatedCrypto,
			// x509ops.ExtKeyUsageNetscapeServerGatedCrypto,
		},
	}
	timestampPrototype := &x509.Certificate{
		KeyUsage: x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageKeyAgreement,
		ExtKeyUsage: []x509.ExtKeyUsage{
			x509.ExtKeyUsageServerAuth,
			x509.ExtKeyUsageOCSPSigning,
			// x509ops.ExtKeyUsageMicrosoftServerGatedCrypto,
			// x509ops.ExtKeyUsageNetscapeServerGatedCrypto,
		},
	}
	cryptoSystems := map[shared.CryptoSystemID]*CryptoSystem{
		CsX509: {
			Name: "X.509",
			Roots: map[shared.CryptoSystemRootID]interface{}{
				X509RootDefault: x509ops.NewRoot(
					settings.CABaseDir,
					"openssl",
					"CA",
					X509RootDefault,
					// TODO: parse crl distribution points from configuration
					[]string{"http://crl.cacert.localhost/revoke.crl"},
					// TODO: parse OCSP endpoints from configuration
					[]string{"http://ocsp.cacert.localhost"},
				),
				X509RootClass3: x509ops.NewRoot(
					settings.CABaseDir,
					"class3",
					"class3",
					X509RootClass3,
					// TODO: parse crl distribution points from configuration
					[]string{"http://crl.cacert.localhost/class3-revoke.crl"},
					// TODO: parse OCSP endpoints from configuration
					[]string{"http://ocsp.cacert.localhost"},
				),
				// The following roots existed in the old server.pl but had
				// no profile configurations and were thus unusable
				//
				// X509RootClass3s: &x509ops.Root{Name: "class3s"},  // no profile configs
				// X509Root3:       &x509ops.Root{Name: "root3"},
				// X509Root4:       &x509ops.Root{Name: "root4"},
				// X509Root5:       &x509ops.Root{Name: "root5"},
			},
			Profiles: map[shared.CertificateProfileID]interface{}{
				X509ProfileClient: x509ops.NewProfile(
					"client",
					clientPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCommonName,
						x509ops.SubjectDnFieldEmailAddress,
					},
					nil,
					true,
				),
				X509ProfileClientOrg: x509ops.NewProfile("client-org", clientPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCountryName,
						x509ops.SubjectDnFieldStateOrProvinceName,
						x509ops.SubjectDnFieldLocalityName,
						x509ops.SubjectDnFieldOrganizationName,
						x509ops.SubjectDnFieldOrganizationalUnitName,
						x509ops.SubjectDnFieldCommonName,
						x509ops.SubjectDnFieldEmailAddress,
					},
					nil,
					true,
				),
				X509ProfileClientCodesign: x509ops.NewProfile("client-codesign", codeSignPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCountryName,
						x509ops.SubjectDnFieldStateOrProvinceName,
						x509ops.SubjectDnFieldLocalityName,
						x509ops.SubjectDnFieldCommonName,
						x509ops.SubjectDnFieldEmailAddress,
					},
					nil,
					true,
				),
				// X509ProfileClientMachine:  &x509ops.Profile{Name: "client-machine"},
				// X509ProfileClientAds:      &x509ops.Profile{Name: "client-ads"},
				X509ProfileServer: x509ops.NewProfile("server", serverPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCommonName,
					},
					[]x509ops.AltNameType{x509ops.NameTypeDNS, x509ops.NameTypeXMPPJid},
					false,
				),
				X509ProfileServerOrg: x509ops.NewProfile("server-org", serverPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCountryName,
						x509ops.SubjectDnFieldStateOrProvinceName,
						x509ops.SubjectDnFieldLocalityName,
						x509ops.SubjectDnFieldOrganizationName,
						x509ops.SubjectDnFieldOrganizationalUnitName,
						x509ops.SubjectDnFieldCommonName,
					},
					[]x509ops.AltNameType{x509ops.NameTypeDNS, x509ops.NameTypeXMPPJid},
					false,
				),
				// X509ProfileServerJabber: &x509ops.Profile{Name: "server-jabber"},
				X509ProfileOCSP: x509ops.NewProfile("ocsp", ocspPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCountryName,
						x509ops.SubjectDnFieldStateOrProvinceName,
						x509ops.SubjectDnFieldLocalityName,
						x509ops.SubjectDnFieldOrganizationName,
						x509ops.SubjectDnFieldOrganizationalUnitName,
						x509ops.SubjectDnFieldCommonName,
						x509ops.SubjectDnFieldEmailAddress,
					},
					nil,
					false,
				),
				X509ProfileTimestamp: x509ops.NewProfile("timestamp", timestampPrototype,
					[]x509ops.SubjectDnField{
						x509ops.SubjectDnFieldCountryName,
						x509ops.SubjectDnFieldStateOrProvinceName,
						x509ops.SubjectDnFieldLocalityName,
						x509ops.SubjectDnFieldOrganizationName,
						x509ops.SubjectDnFieldOrganizationalUnitName,
						x509ops.SubjectDnFieldCommonName,
					},
					nil,
					true,
				),
				// X509ProfileProxy:          &x509ops.Profile{Name: "proxy"},
				// X509ProfileSubCA:          &x509ops.Profile{Name: "subca"},
			},
			// constants for openssl invocations. Should be replaced with
			// something more useful
			DigestAlgorithms: map[shared.SignatureAlgorithmID]interface{}{
				X509MDDefault: x509.SHA256WithRSA,
				X509MDMd5:     x509.MD5WithRSA,
				X509MDSha1:    x509.SHA1WithRSA,
				X509MDSha256:  x509.SHA256WithRSA,
				X509MDSha384:  x509.SHA384WithRSA,
				X509MDSha512:  x509.SHA512WithRSA,
			},
		},
		CsOpenPGP: {
			Name: "OpenPGP",
			Roots: map[shared.CryptoSystemRootID]interface{}{
				OpenPGPRoot0: &openpgpops.OpenPGPRoot{
					Name: "OpenPGP Root",
					SecretKeyRing: path.Join(
						settings.OpenPGPKeyRingDir,
						fmt.Sprintf("gpg_root_%d", OpenPGPRoot0),
						"secring.gpg",
					),
					Identifier: settings.OpenPGPUidEmail,
				},
			},
			Profiles: map[shared.CertificateProfileID]interface{}{
				OpenPGPDefaultProfile: &openpgpops.OpenPGPProfile{Name: "default"},
			},
			// constants for gnupg cert-digest-algo parameter. Should be replaced with
			// something more useful
			DigestAlgorithms: map[shared.SignatureAlgorithmID]interface{}{
				OpenPGPDefaultMD: crypto.SHA256,
			},
		},
	}

	return &CommandProcessor{CryptoSystems: cryptoSystems, Settings: settings}
}
