package signer

import (
	"git.cacert.org/cacert-gosigner/shared"
)

type CryptoSystem struct {
	Name             string
	Roots            map[shared.CryptoSystemRootID]interface{}
	Profiles         map[shared.CertificateProfileID]interface{}
	DigestAlgorithms map[shared.SignatureAlgorithmID]interface{}
}

func (system CryptoSystem) String() string {
	return system.Name
}
