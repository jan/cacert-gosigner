module git.cacert.org/cacert-gosigner

go 1.15

require (
	github.com/ProtonMail/go-crypto v0.0.0-20210512092938-c05353c2d58c
	github.com/longsleep/pkac v0.0.0-20191013204540-205111305195
	github.com/sirupsen/logrus v1.7.0
	go.bug.st/serial v1.1.1
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68
	gopkg.in/yaml.v2 v2.4.0
)

// replace golang.org/x/crypto => github.com/jandd/crypto v0.0.0-20210106144236-c3a8dd255ad6
