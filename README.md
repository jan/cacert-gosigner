# CAcert signer in Go

This is a reimplementation of the CAcert signer code from the
[CommModule directory](https://git.cacert.org/gitweb/?p=cacert-devel.git;a=tree;f=CommModule) of the CAcert software
in [Go](https://golang.org/). The goal of this effort is to provide a more maintainable version of the software.

## Running the signer and client locally

The signer is usually attached to a USB serial port. You can run the signer and client locally using the
[socat](https://manpages.debian.org/buster/socat/socat.1.en.html) utility.

```shell script
socat -d -d PTY,link=$(pwd)/ttyS0 PTY,link=$(pwd)/ttyS1
```

You may then run the server

```shell script
go run signer/main.go -a $(pwd)/dev/ttyS0
```

and the client:

```shell script
go run client/main.go -a $(pwd)/dev/ttyS1
```