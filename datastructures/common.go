package datastructures

import "encoding/binary"

const signerTimeFormat = "010203042006.05"

func Encode24BitLength(data []byte) []byte {
	lengthBytes := make([]byte, 4)
	binary.BigEndian.PutUint32(lengthBytes, uint32(len(data)))

	return lengthBytes[1:]
}

// calculate length from 24 bits of data in network byte order
func Decode24BitLength(bytes []byte) int {
	return int(binary.BigEndian.Uint32([]byte{0x0, bytes[0], bytes[1], bytes[2]}))
}

func CalculateXorCheckSum(byteBlocks [][]byte) byte {
	var result byte = 0x0

	for _, byteBlock := range byteBlocks {
		for _, b := range byteBlock {
			result ^= b
		}
	}

	return result
}
