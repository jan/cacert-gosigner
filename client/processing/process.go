package processing

import (
	"fmt"

	"git.cacert.org/cacert-gosigner/datastructures"
	"git.cacert.org/cacert-gosigner/shared"

	"github.com/sirupsen/logrus"
)

func Process(response *datastructures.SignerResponse) (err error) {
	logrus.Infof("process response of type %s", response.Action)
	logrus.Tracef("process response %+v", response)

	switch response.Action {
	case shared.ActionNul:
		logrus.Trace("received response for NUL request")

		return
	default:
		return fmt.Errorf("unsupported action in response 0x%x", response.Action)
	}
}
