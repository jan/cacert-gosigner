package main

import (
	"bytes"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
)

func TestGenerateExampleConfig(t *testing.T) {
	testOutput := bytes.Buffer{}
	log.SetOutput(&testOutput)

	config, err := generateExampleConfig("test.yaml")

	if err != nil {
		t.Errorf("unexpected error %v", err)
	}

	if config.SerialAddress != "/dev/ttyUSB0" {
		t.Errorf("got %s instead of expected serial address /dev/ttyUSB0", config.SerialAddress)
	}

	if config.BaudRate != 115200 {
		t.Errorf("got %d instead of expected baud rate 115200", config.BaudRate)
	}

	logOutput := testOutput.String()
	if !strings.Contains(logOutput, "test.yaml") {
		t.Error("logged instructions do not contain the file name test.yaml")
	}

	if !strings.Contains(logOutput, "---\\n") {
		t.Errorf("logged instructions do not contain a yaml prefix: \n%s", logOutput)
	}

	yamlKeys := []string{"serial_address", "serial_baudrate", "paranoid", "debug", "gnupg_bin", "openssl_bin", "mysql_dsn"}
	for _, item := range yamlKeys {
		if !strings.Contains(logOutput, item) {
			t.Errorf("%s missing in example log output", item)
		}
	}
}

func TestReadConfigNonExistingFile(t *testing.T) {
	config, err := readConfig("nonExistant.yaml")

	if err != nil {
		t.Errorf("unexpected error %v", err)
	}

	exampleConfig, _ := generateExampleConfig("nonExistant.yaml")
	if config != exampleConfig {
		t.Errorf("expected example config but got %+v", config)
	}
}
