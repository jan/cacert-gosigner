package main

import (
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"go.bug.st/serial"
	"gopkg.in/yaml.v2"
)

const (
	dataBits          = 8
	defaultBaudRate   = 115200
	defaultBufferSize = 2048
)

type ClientConfig struct {
	SerialAddress string `yaml:"serial_address"`
	BaudRate      int    `yaml:"serial_baudrate"`
	BufferSize    uint16 `yaml:"serial_buffer_size"`
	Paranoid      bool   `yaml:"paranoid"`
	Debug         bool   `yaml:"debug"`
	GNUPGBinary   string `yaml:"gnupg_bin"`
	OpenSSLBinary string `yaml:"openssl_bin"`
	MySQLDSN      string `yaml:"mysql_dsn"`
}

var defaultConfig = ClientConfig{
	SerialAddress: "/dev/ttyUSB0",
	BaudRate:      defaultBaudRate,
	BufferSize:    defaultBufferSize,
	Paranoid:      false,
	Debug:         false,
	OpenSSLBinary: "/usr/bin/openssl",
	GNUPGBinary:   "/usr/bin/gpg",
	MySQLDSN:      "<username>:<password>@/database?parseTime=true",
}

func generateExampleConfig(configFile string) (*ClientConfig, error) {
	config := &defaultConfig

	configBytes, err := yaml.Marshal(config)
	if err != nil {
		return nil, fmt.Errorf("could not generate configuration data: %w", err)
	}

	log.Infof("example data for %s:\n\n---\n%s\n", configFile, configBytes)

	return config, nil
}

func readConfig(configFile string) (config *ClientConfig, err error) {
	source, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Errorf("opening configuration file failed: %v", err)

		exampleConfig, err := generateExampleConfig(configFile)
		if err != nil {
			return nil, err
		}

		log.Info("starting with default config")

		return exampleConfig, nil
	}

	if err := yaml.Unmarshal(source, &config); err != nil {
		return nil, fmt.Errorf("loading configuration file failed: %w", err)
	}

	return config, nil
}

func fillSerialMode(clientConfig *ClientConfig) *serial.Mode {
	return &serial.Mode{
		BaudRate: clientConfig.BaudRate,
		DataBits: dataBits,
		StopBits: serial.OneStopBit,
		Parity:   serial.NoParity,
	}
}
