package shared

const (
	ProtocolVersion = 1

	HandshakeByte = 0x02
	AckByte       = 0x10
	ResendByte    = 0x11

	MagicTrailer = "rie4Ech7"

	LengthFieldSize   = 3
	CheckSumFieldSize = 1
	TrailerFieldSize  = len(MagicTrailer)
)

type Action byte

const (
	ActionNul    = Action(0)
	ActionSign   = Action(1)
	ActionRevoke = Action(2) // nolint:gomnd
)

func (a Action) String() string {
	switch a {
	case ActionNul:
		return "NUL"
	case ActionSign:
		return "SIGN"
	case ActionRevoke:
		return "REVOKE"
	default:
		return "unknown"
	}
}

type CryptoSystemRootID byte

type CertificateProfileID byte

type SignatureAlgorithmID byte

type CryptoSystemID byte
