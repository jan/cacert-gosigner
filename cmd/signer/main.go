package main

import (
	"flag"
	"os"
	"os/signal"

	log "github.com/sirupsen/logrus"
	"go.bug.st/serial"

	"git.cacert.org/cacert-gosigner/signer"
)

func main() {
	var (
		address            string
		baudRate, dataBits int
	)

	flag.StringVar(&address, "a", "/dev/ttyUSB0", "address")
	flag.IntVar(&baudRate, "b", 115200, "baud rate")
	flag.IntVar(&dataBits, "d", 8, "data bits")
	flag.Parse()

	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	log.SetLevel(log.TraceLevel)

	serialMode := &serial.Mode{
		BaudRate: baudRate,
		DataBits: dataBits,
		StopBits: serial.OneStopBit,
		Parity:   serial.NoParity,
	}

	log.Infof("connecting to %s using %+v", address, serialMode)

	port, err := serial.Open(address, serialMode)
	if err != nil {
		log.Fatalf("could not open serial port: %v", err)
	}

	log.Info("connected")

	defer func() {
		err := port.Close()
		if err != nil {
			log.Fatalf("could not close port: %v", err)
		}

		log.Info("serial port closed")
	}()

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		log.Info("server is shutting down...")

		close(done)
	}()

	go func() {
		signerProcess := signer.NewSignerProcess(port)
		signerProcess.MainLoop()

		close(quit)
	}()

	<-done
	log.Infoln("server stopped")
}
